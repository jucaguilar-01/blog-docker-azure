## project
Information blog about azure and docker technologies
done with the help of Node.js (express) + pug template project, using Twitter Bootstrap.

#source code
https://gitlab.com/jucaguilar-01/blog-docker-azure.git

## Usage
- Clone repository.
- Open a command prompt, navigate to the folder, and enter: npm install
- Next, run the app by entering: npm start
- Browse to http://localhost:3000

## demo
https://blog-azure-docker.herokuapp.com

## Author
aguilar juan carlos