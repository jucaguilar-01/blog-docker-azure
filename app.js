
/**
 * Module dependencies.
 */

var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.get("/", function(req, res) {
  res.render("index");
});

app.get("/docker", function(req, res) {
  res.render("layouts/docker-home");
});

app.get("/azure", function(req, res) {
  res.render("layouts/azure-home");
});

app.get("/docker/11", function(req, res) {
  res.render("layouts/docker-11");
});

app.get("/docker/images", function(req, res) {
  res.render("layouts/docker-images");
});

app.get("/docker/persistence", function(req, res) {
  res.render("layouts/docker-persistence");
});

app.get("/docker/networks", function(req, res) {
  res.render("layouts/docker-networks");
});


app.get("/docker/dockerfile", function(req, res) {
  res.render("layouts/docker-file");
});

app.get("/docker/compose", function(req, res) {
  res.render("layouts/docker-compose");
});

app.get("/azure/fundaments", function(req, res) {
  res.render("layouts/azure-fundaments");
});

app.get("/azure/vm", function(req, res) {
  res.render("layouts/azure-virtual-machine");
});

app.get("/azure/networks/", function(req, res) {
  res.render("layouts/azure-networks");
});

app.get("/azure/balance/", function(req, res) {
  res.render("layouts/azure-balance");
});

app.get("/azure/administration/", function(req, res) {
  res.render("layouts/azure-administration");
});

app.get("/azure/docker", function(req, res) {
  res.render("layouts/azure-on-docker");
});

app.use('/', indexRouter);
app.use('/users', usersRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
